This is a demo project that uses Spring, MySQL, hibernate and maven. It supplies REST API that preform CRUD on a SHOPPER entity

* In order to be ale to run the project you should first create a new schema in your mysql DB called `API`:
create database `API`;

and a table called SHOPPERS:

CREATE TABLE `shoppers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `address1` varchar(45) DEFAULT NULL,
  `address2` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `zip` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `preferred_currency` varchar(45) DEFAULT NULL,
  `owner` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

In the `authorities` table the field `username` should be a foreign key (taken from USERS) but since it's only a demo we'll declare it as follows:
 
CREATE TABLE `authorities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `authority` varchar(45) CHARACTER SET latin1 NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf16;

and for Spring security to work "out of the box" we also need to make sure there's a boolean field called `enabled` in USERS table:

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `last_name` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `username` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `password` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `email` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf16;
 
don't forget to insert some values...
The field `SHOPPERS.owner` should carry the `username` (from table USERS) of the user that created this shopper, otherwise you're request will be 403 denided 

* change the DB connection object (dataSource) in rest-servlet.xml to include your username/password

* and we're ready!

* example of usage:

1. get shopper information (id = 18):

HTTP GET:
http://alfasin:1234@localhost:8080/shopper/18.xml
or
http://alfasin:1234@localhost:8080/shopper/18.json
or:
http://alfasin:1234@localhost:8080/shopper/18
(defaults to XML)

in every HTTP request you should use the headers:
Content-type: application/xml
Accept: application/xml

or:

Content-type: application/json
Accept: application/json

or any combination.
__________________________________

2. add a new shopper to the system:

INSERT (supports both HTTP POST/PUT methods):
url:
http://alfasin:1234@localhost:8080/shopper/insert

headers:
Content-Type: application/json

body:
{"firstName":"Nir","lastName":"Alfasi","address1":"999 Main st.","address2":"","city":"San Jose","state":"CA","country":"USA","zip":"95134","email":"alfasin@gmail.com","phone":"408-8888888","owner":"alfasin"}

__________________________________

3. update an existing shopper information:

UPDATE (supports both HTTP POST/PUT methods):
url:
http://alfasin:1234@localhost:8080/shopper/update/12

headers:
Content-Type: application/json

body:
{"id":"12","firstName":"Nir","lastName":"Alfasi","address1":"999 Main st.","address2":"","city":"San Jose","state":"CA","country":"USA","zip":"95134","email":"alfasin@gmail.com","phone":"408-8888888","owner":"alfasin"}

or:
<shopper><id>1</id><firstName>Nir</firstName><lastName>Alfasi</lastName><address1>999 Main st.</address1><address2></address2><city>San Jose</city><state>CA</state><country>USA</country><zip>95134</zip><email>alfasin@gmail.com</email><phone>408-888-8888</phone><owner>alfasin</owner></shopper>

[![Codacy Badge](https://api.codacy.com/project/badge/grade/100c1d0b03d347aaa664c50bf8ad3890)](https://www.codacy.com/app/alfasin/shopper-rest-api-demo)
