package com.alfasin.rest.service;

import com.alfasin.rest.dao.ShopperHibernateDaoImpl;
import com.alfasin.rest.model.Shopper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * User: alfasin
 * Date: 8/15/13
 */
@Transactional
@Service
public class ShopperService {

    @Autowired
    private ShopperHibernateDaoImpl shopperDao;

    public Shopper getShopper(Integer shopperId){
        Shopper shopper = shopperDao.getShopper(shopperId);
        return shopper;
    }

    public Shopper updateShopper(Shopper shopper) {
        return shopperDao.updateShopper(shopper);
    }

    public Shopper insertShopper(Shopper shopper) {
        shopperDao.insertShopper(shopper);
        return shopper;
    }
}
