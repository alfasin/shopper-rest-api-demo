package com.alfasin.rest.controller;

import com.alfasin.rest.model.Shopper;
import com.alfasin.rest.service.ShopperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * User: alfasin
 * Date: 7/27/13
 */
@Controller
@RequestMapping("/shopper")
public class ShopperController {

    @Autowired
    private ShopperService shopperService;

    @PostAuthorize("returnObject.owner == principal.username")
    @RequestMapping(value = "/{shopperId}",method = { RequestMethod.GET })
    public Shopper getShopper(@PathVariable(value="shopperId") Integer shopperId){
        Shopper shopper = shopperService.getShopper(shopperId);
        return shopper;
    }

    @PostAuthorize("returnObject.owner == principal.username")
    @RequestMapping(value = "/update/{id}",method = { RequestMethod.PUT, RequestMethod.POST })
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void updateShopper(@PathVariable Integer id, @RequestBody Shopper shopper) {
        //validation
        if(id != shopper.getId()){
            throw new NoSuchElementException("No such resource: "+id);
        }
        shopperService.updateShopper(shopper);
    }

    @PostAuthorize("returnObject.owner == principal.username")
    @RequestMapping(value = "/insert",method = { RequestMethod.PUT, RequestMethod.POST })
    @ResponseStatus(value = HttpStatus.CREATED)
    public Shopper insertShopper(@RequestBody Shopper shopper) {
        shopperService.insertShopper(shopper);
        return shopper;
    }
}
