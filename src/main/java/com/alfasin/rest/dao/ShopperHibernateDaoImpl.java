package com.alfasin.rest.dao;

import com.alfasin.rest.model.Shopper;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * User: alfasin
 * Date: 8/13/13
 */
@Component
public class ShopperHibernateDaoImpl implements ShopperDao{


    @Autowired
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Shopper getShopper(Integer shopperId) {
        return (Shopper) sessionFactory.getCurrentSession().get(Shopper.class, shopperId);
    }

    @Override
    public Shopper insertShopper(Shopper shopper) {
        Integer shopperId = (Integer)sessionFactory.getCurrentSession().save(shopper);
        System.out.println("shopperId: "+shopperId);
        return getShopper(shopperId);
    }

    @Override
    public Shopper updateShopper(Shopper shopper) {
        sessionFactory.getCurrentSession().update(shopper);
        return getShopper(shopper.getId());
    }

}
