package com.alfasin.rest.dao;

import com.alfasin.rest.model.Shopper;

/**
 * User: alfasin
 * Date: 7/27/13
 */
public interface ShopperDao {

    public Shopper getShopper(Integer shopperId);
    public Shopper insertShopper(Shopper shopper);
    public Shopper updateShopper(Shopper shopper);

}
