package com.alfasin.rest.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.persistence.*;
import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * User: alfasin
 * Date: 7/27/13
 */
@Entity
@Table(name="SHOPPERS")
@XStreamAlias("shopper")
@XmlRootElement(name = "shopper")
@XmlAccessorType(XmlAccessType.FIELD)
public class Shopper  implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="ID", nullable = false)
    @XmlElement(required = false)
    public Integer id;

    @Column(name="FIRST_NAME", nullable = false)
    @XmlElement
    public String firstName;

    @Column(name="LAST_NAME", nullable = false)
    @XmlElement
    public String lastName;

    @Column(name="ADDRESS1", nullable = false)
    @XmlElement
    public String address1;

    @Column(name="ADDRESS2", nullable = true)
    @XmlElement
    public String address2;

    @Column(name="CITY", nullable = false)
    @XmlElement
    public String city;

    @Column(name="STATE", nullable = false)
    @XmlElement
    public String state;

    @Column(name="COUNTRY", nullable = false)
    @XmlElement
    public String country;

    @Column(name="ZIP", nullable = false)
    @XmlElement
    public String zip;

    @Column(name="EMAIL", nullable = false)
    @XmlElement
    public String email;

    @Column(name="PHONE", nullable = false)
    @XmlElement
    public String phone;

    @Column(name="PREFERRED_CURRENCY", nullable = false)
    @XmlElement
    public String preferredCurrency;

    @Column(name="OWNER", nullable = false)
    @XmlElement
    public String owner;

    @Override
    public String toString() {
        return "Shopper [" +
                "firstName="+ firstName
                +"lastName="+ lastName
                +"address1="+address1
                +"address2="+address2
                +"city="+city
                +"state="+state
                +"country="+country
                +"zip="+zip
                +"email="+email
                +"phone="+phone
                +"preferredCurrency="+ preferredCurrency
                +"owner=" + owner +
                "]";
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPreferredCurrency() {
        return preferredCurrency;
    }

    public void setPreferredCurrency(String preferredCurrency) {
        this.preferredCurrency = preferredCurrency;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

}
